from saport.simplex.solver import Solver
from saport.simplex.model import Model

model = Model("zadanie 3")


s = model.create_variable("portion of steak")
p = model.create_variable("portion of potatoes")

model.add_constraint(15*s + 2*p <= 60)
model.add_constraint(5*s + 15 * p >= 50)
model.add_constraint(20*s + 5*p >= 40)

model.minimize(8*s + 4*p)


model.solve(Solver())
